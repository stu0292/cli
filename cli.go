// package cli provides some helper funcs to get a rudimentary cli up and running.
package cli

import (
	"bufio"
	"flag"
	"fmt"
	"os"
)

// Parse parses flags and checks if user has requested help, in which case usage text is offered
func Parse(usage string) {

	// update flag usage func
	flag.Usage = func() {
		fmt.Printf("%s:", os.Args[0])
		fmt.Print(usage)
		flag.PrintDefaults()
	}

	// parse flags if not done already
	if !flag.Parsed() {
		flag.Parse()
	}

	if NeedsHelp(flag.Arg(0)) {
		// flag.ErrHelp checks incase -h has been called by user but not defined - but seems to be always not nil?
		//|| flag.ErrHelp != nil {
		flag.Usage()
		os.Exit(1)
	}
}

// NeedsHelp checks for common ways a user might try to request help from a cli
// helpArg: usually the first user argument. Compared to strings that a user may commonly try when requesting help from the tool.
func NeedsHelp(helpArg string) bool {
	return helpArg == "help" ||
		helpArg == "-help" ||
		helpArg == "--help" ||
		helpArg == "-h"
}

// Arg returns the ith cli arg, wraps flag.Arg.
// If the return value is blank, defaultValue is returned.
func Arg(i int, defaultValue string) string {

	s := flag.Arg(i)

	// default if blank.
	if s == "" {
		s = defaultValue
	}
	return s
}

// Marg (mandatory arg) returns the ith cli arg, wraps flag.Arg.
// If the return value is blank, msg is printed then the program exits.
func Marg(i int, msg string) string {

	s := flag.Arg(i)

	// return non-blank args
	if s != "" {
		return s
	}

	// fatal otherwise
	if msg == "" {
		msg = fmt.Sprintf("Error: Mandatory argument %v is missing", i)
	}
	fmt.Fprintln(os.Stderr, msg)
	flag.Usage()
	os.Exit(1)

	return ""
}

// Fatal prints msg to STDERR and terminates program.
func Fatal(msg ...interface{}) {
	fmt.Fprint(os.Stderr, msg...)
	os.Exit(1)
}

// FileExists checks if a file or folder already exists so the user can be prompted for overwrite
func FileExists(filePath string) bool {

	// try to Stat the file then check if a NotExist error was thrown
	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		return false
	} else {
		return true
	}
}

// Prompt prompts the user with message and reads STDIN until \\n character is entered
func Prompt(message string) string {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print(message)
	input, _ := reader.ReadString('\n')

	return input
}
