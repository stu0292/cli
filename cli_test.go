package cli

import (
  th "gitlab.com/stu-b-doo/thelper"
  te "testing"
  "fmt"
  "flag"
)

const usage = `Usage text`

func Example() {

	// define flags 
	flagA := flag.String("A", "DefaultVal", "First flag")
	flagB := flag.String("B", "DefaultVal", "Second flag")

	// Parse args and offer usage text if requested
	Parse(usage)

	// read mandatory args first
	arg0 := Marg(0, "Error: first mandatory argument missing")

	// then optional args
	arg1 := Arg(1, "defaultValue")

	// call your business functions...
	fmt.Println(flagA, flagB, arg0, arg1)

	// Output: 
}

func ExamplePrompt() {

	fmt.Println("You entered:", Prompt("Enter  text: "))
	Prompt("Press any key to exit...")
}

func TestFileExists(t *te.T) {

  // Test cases: 
  cases := map[string]bool{
    ".git": true,
    ".git/HEAD": true,
    "cli_test.go": true,
    "banana": false,
  }

  for f, exists := range cases {

    th.Eq(t, exists, FileExists(f), "Does file %v exist?", f)
  }

}

func TestOfferHelp(t * te.T) {

  // Test cases: 
  cases := map[string]bool{
    "help": true,
    "-help": true,
    "--help": true,
    "-h": true,
    "asdflkj": false,
    "": false,
  }

  for c, b := range cases {

    th.Eq(t, b, NeedsHelp(c), "Test case: '%v', helpFlag: false", c)
  }
}
