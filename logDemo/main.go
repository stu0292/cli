// log demo
package main

import (
	"log"
	"os"
	"io/ioutil"
	"fmt"
)

func main() {
	// decided to log the standard logger to a file rather than Stderr
	logfile := "my.log"
	f, err := os.OpenFile(logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644); if err != nil {
		log.Println(err)
	}
	defer f.Close()
	log.SetOutput(f)

	// Initial log entry with date, time, program name
	log.Println(os.Args[0])

	// subsequent logs - only interested in Ltime
	log.SetFlags(log.Ltime)

	// prefix
	log.SetPrefix("Log demo: ")

	// start logging (to file)
	log.Println("hello")

	// log back to STDERR rather than file
	log.SetOutput(os.Stderr)
	log.Println("world")

	// no flags/prefix
	log.SetFlags(0)
	log.SetPrefix("")
	log.Println("No timestamp/prefix")

	// disable log
	log.SetOutput(ioutil.Discard)
	log.Println("This text not logged")

	// print file logged contents
	// Note file is in append mode (subsequent runs should see corresponding timestamps)
	b, _ := ioutil.ReadFile(logfile)
	fmt.Printf("\nlog file '%v':\n%s", logfile, b)
}
