package cli provides some helper funcs to get a rudimentary cli up and running in [Go](https://golang.org).

See [GoDoc](https://godoc.org/gitlab.com/stu-b-doo/cli)

Things I might do one day in [todo.md](todo.md)

If you need something more powerful and mainstream, github.com/spf13/cobra comes recommended.

# main file boilerplate

```
package main

import (
  "gitlab.com/stu-b-doo/cli"
  "flag"
  "fmt"
)

conts usage = `Usage text`

func main() {

	// define flags 
	flagA := flag.String("A", "DefaultVal", "First flag")
	flagB := flag.String("B", "DefaultVal", "Second flag")

	// Parse args and offer usage text if requested
	cli.Parse(usage)

	// read mandatory args first
	arg0 := cli.Marg(0, "Error: first mandatory argument missing")

	// then optional args
	arg1 := cli.Arg(1, "defaultValue")

	// call your business functions...
	fmt.Println(flagA, flagB, arg0, arg1)
}
```

# User prompting

```
fmt.Println("You entered:", cli.Prompt("Enter  text: "))
cli.Prompt("Press any key to exit...")
```

# Output and logging

Demo of the standard logging library in [logDemo](logDemo/main.go)

