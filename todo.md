# CLI todo

- DefineArgs (is this much different to what flags is doing?)

```
type ArgDef struct{
	name
	description
	mandatory
	default
}
type Args struct{
	args []ArgDef
} 
func cli.Parse(usage, []ArgDef) Args
// uses arg defs to construct usage message including arg descriptions, defaults and if they're mandatory or not, accompanied extra usage message
// fatal if mandatory arg not supplied or if mandatory args aren't all listed before optional args
// other stuff cli.Parse currently does

// Get returns the value of supplied arg or default if optional arg not supplied
func (a Args) Get(n int) string 
```
	

- https://stackoverflow.com/questions/22744443/check-if-there-is-something-to-read-on-stdin-in-golang to check if STDIN?

